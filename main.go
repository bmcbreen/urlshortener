package main

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/bmcbreen/URLShortener/pkg/configs"
	"gitlab.com/bmcbreen/URLShortener/pkg/utils"
)

// @title URLShortener
// @version 1.0
// @description Simple URL shortening web application
func main() {
	config := configs.FiberConfig()

	app := fiber.New(config)

	utils.StartServer(app)
}
