# URL Shortener

## Description
URL Shortener is a simple URL shortening application written in Go using the Fiber HTTP framework and HTMX for
the frontend. It uses PostgreSQL for storing the URLs and Redis for caching popular URLs.

## Technologies
- [Go](https://golang.org/) - 1.21
- [Fiber](https://gofiber.io/) - 2.52
- [PostgreSQL](https://www.postgresql.org/) - 16.1
- [Redis](https://redis.io/) - 7.2.4
- [Docker](https://www.docker.com/) - 24.0.7
- [HTMX](https://htmx.org/) - 1.9.10

## Credits
- [create-go-app/fiber-go-template](https://github.com/create-go-app/fiber-go-template) - I used this as a 
starting point for my Fiber project structure.

## License
URL Shortener is licensed under GNU GPLv3. See [LICENSE](LICENSE) for more information.