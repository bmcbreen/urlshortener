package database

import (
	"context"
	"github.com/jackc/pgx/v5"
	"gitlab.com/bmcbreen/URLShortener/pkg/utils"
)

func GetConnection(ctx context.Context) (*pgx.Conn, error) {
	url, err := utils.ConnectionURLBuilder("postgres")
	if err != nil {
		return nil, err
	}
	return pgx.Connect(ctx, url)
}
