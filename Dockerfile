FROM golang:1.21 as builder

WORKDIR /app

COPY . .

CMD ["go", "build", "."]

FROM alpine:latest

WORKDIR /app