package utils

import (
	"github.com/gofiber/fiber/v2"
	"log"
)

func StartServer(a *fiber.App) {
	url, _ := ConnectionURLBuilder("fiber")

	if err := a.Listen(url); err != nil {
		log.Printf("Unable to start server: %v\n", err)
	}
}
