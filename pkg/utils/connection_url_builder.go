package utils

import (
	"fmt"
	"os"
)

func ConnectionURLBuilder(n string) (string, error) {
	var url string
	switch n {
	case "postgres":
		url = fmt.Sprintf(
			"postgres://%s:%s@%s:%s/%s",
			os.Getenv("POSTGRES_HOST"),
			os.Getenv("POSTGRES_PORT"),
			os.Getenv("POSTGRES_USER"),
			os.Getenv("POSTGRES_PASSWORD"),
			os.Getenv("POSTGRES_DB"))
	case "redis":
		url = fmt.Sprintf(
			"%s:%s",
			os.Getenv("REDIS_HOST"),
			os.Getenv("REDIS_PORT"))
	case "fiber":
		url = fmt.Sprintf(
			"%s:%s",
			os.Getenv("FIBER_HOST"),
			os.Getenv("FIBER_PORT"))
	default:
		return "", fmt.Errorf("invalid connection name")
	}

	return url, nil
}
